<?php

// int print ( string $arg )

/*
print no es realmente una función (es un constructor de lenguaje)
por lo tanto no es necesario usar paréntesis para indicar su lista de
argumentos.
*/

//Question 1
echo '1' . print('2')+3;
echo "<br>";
echo '1' . (print '2') + 3;
echo "<br>";
print(2);
echo "<br>";
print "print() tambien funciona sin parentesis.";
echo "<br>";
$foo = "foobar";
print "foo es $foo"; // foo es foobar
echo "<br>";
$bar = array("value" => "foo");
print "Esto es {$bar['value']} !"; // Esto es foo !
echo "<br>";
print $foo;
echo "<br>";
echo '1'+'2';
echo "<br>";
echo '1'.'2';
echo "<br>";
print 2 ;
echo "<br>";
echo print ('a');
echo "<br>";
echo (print  'a');
echo "<br>";
echo '7'.(print '2') + 3;
?>