<?

/*
Operadores bit a bit ¶
Los operadores bit a bit permiten la evaluación y la manipulación de bits específicos dentro de un integer.


$a & $b - and
$a | $b - or
$a ^ $b - xor -> Los bits que están activos en $a o en $b, pero no en ambos, son activado
~ $a - not
$a << $b - shift left
$a >> $b - shift right

*/

echo 1 ^  2;
echo "<br>";
print(1 ^ 2);
echo "<br>";
echo 2 ^ 2;
echo "<br>";
echo 3 ^ 3;
echo "<br>";
echo 6 ^ 3;
echo "<br>";
echo 3 & 3;
echo "<br>";
echo 3 | 5;
echo "<br>";
echo 5 ^ 3;
echo "<br>";
echo ~2;
echo "<br>";
echo 5 << 2;
echo "<br>";
echo 7 >> 3;
?>