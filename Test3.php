<?php

//instanceof is an operator -> language construct
//is_a is a function

class Shape{

}

class Animal{

}

class Circle extends Shape{

}

$shape = new Shape();
var_dump($shape instanceof Shape); // true
echo "<br>";

$circle = new Circle();

var_dump($circle instanceof Shape); // true
echo "<br>";
var_dump($circle instanceof Circle); // true
echo "<br>";
var_dump($circle instanceof Animal); //false

?>