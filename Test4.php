<?

// Las variables escalares son aquellas que contienen un integer, float,
// string o boolean. Tipos array, object y resource no son escalares.


// Las formas más básicas de expresiones son las constantes y
// las variables. Cuando se escribe "$a = 5", se está asignando '5' a $a.

$var = 'a';

switch ($var) {
    case 1:
        # code...
        echo "numero";
        break;
    case 'a':
        # code...
        echo "letra";
        break;
    default:
        # code...
        break;
}

?>